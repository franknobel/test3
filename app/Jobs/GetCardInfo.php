<?php

namespace App\Jobs;

use App\Models\ClientFilesElms;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Imports\ClientFilesImport;
use App\Models\ClientFiles;
use App\Http\Services\Clients\BankCardsService;
use Illuminate\Support\Facades\Log;

class GetCardInfo implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    protected int $clientFileId ;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($clientFileId)
    {
        $this->clientFileId = $clientFileId;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $bankCardsService = new BankCardsService();
        
        $clientFilesElements = ClientFilesElms::whereClientFilesId($this->clientFileId)->get();

        foreach($clientFilesElements as $element) {
            
            $cutCard = $this->cutCard($element->card);
            $record = $bankCardsService->getCardInfo($cutCard);

            if(null  === $record) {
                // Can write logs
                continue;
            }
            
            $element->type = $record['data']['card']['scheme'] . ' ' . $record['data']['card']['category'];
            $element->bank = $record['data']['bank']['name'];
            $element->update();

        }

        // Create job for send file to client
        $bankCardsService->SendClientFile($this->clientFileId);

    }

    private function cutCard(string $card) 
    {
        return substr($card, 0, 6);
    }
}
