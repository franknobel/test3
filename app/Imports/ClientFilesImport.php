<?php

namespace App\Imports;

use App\Models\ClientFiles;
use App\Models\ClientFilesElms;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Facades\Validator;

class ClientFilesImport implements ToCollection, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return int
    */
    public function collection(Collection $rows): int
    {
        $hash = md5_file(request()->file()['file']);
        $userId = auth()->user()->id;

        Validator::make($rows->toArray(), [
             '*.id' => 'required',
             '*.info' => 'required',
             '*.number' => 'required',
        ])->validate();
  
        $clientFile = ClientFiles::create([
            'user_id' => $userId,
            'hash' => $hash
        ]);

        foreach ($rows as $row) {
            ClientFilesElms::create([
                'client_files_id' => $clientFile->id,
                'client_id' => $row['id'],
                'card' => $row['number'],
                'info' => $row['info']
            ]);
        }

        return $clientFile->id;
    }
}
