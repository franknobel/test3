<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientFiles extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'hash'
    ];
}
