<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ClientFilesElms extends Model
{
    use HasFactory;

    protected $fillable = [
        'client_files_id',
        'client_id',
        'info',
        'card',
        'type',
        'bank'
    ];
}
