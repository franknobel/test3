<?php
   
namespace App\Http\Controllers\Api\Users;
   
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use App\Http\Helpers\ResponseHelper; 
use App\Http\Services\Users\RegisterService;  

class RegisterController extends Controller
{
    /**
     * @var RegisterService
     */
    private RegisterService $registerService;

    public function __construct(
        RegisterService $registerService
    )
    {
        $this->registerService = $registerService;
    }

    /**
     * Register api
     *
     * @return JsonResponse
     */
    public function register(): JsonResponse
    {
        return $this->getRegisterService()->register();
    }
   
    /**
     * Login api
     *
     * @return JsonResponse
     */
    public function login(): JsonResponse
    {
        return $this->getRegisterService()->login();
    }

    /**
     * @return RegisterService
     */
    private function getRegisterService(): RegisterService
    {
        return $this->registerService;
    }
}
