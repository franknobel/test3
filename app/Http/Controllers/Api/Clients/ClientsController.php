<?php
   
namespace App\Http\Controllers\Api\Clients;

use App\Http\Controllers\Controller;
use App\Http\Services\Clients\BankCardsService;

class ClientsController extends Controller
{
    /**
     * @var BankCardsService
     */
    private BankCardsService $bankCardService;

    public function __construct(BankCardsService $bankCardService) 
    {
        $this->bankCardService = $bankCardService;
    }

    public function checkBankCards()
    {
        return $this->getBankCardsService()->checkBankCards();
    }

    /**
     * @return BankCardsService
     */
    private function getBankCardsService(): BankCardsService
    {
        return $this->bankCardService;
    }
}