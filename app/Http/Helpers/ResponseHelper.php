<?php

namespace App\Http\Helpers;

use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

trait ResponseHelper
{
    /**
     * success response method.
     *
     * @return JsonResponse
     */
    public function sendResponse($result, $message): JsonResponse
    {
    	$response = [
            'success' => true,
            'data'    => $result,
            'message' => $message,
        ];

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return JsonResponse
     */
    public function sendError($error, $errorMessages = [], $code = 400): JsonResponse
    {
    	$response = [
            'success' => false,
            'message' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }
}