<?php

namespace App\Http\DTO\Users\Register;

use Spatie\DataTransferObject\DataTransferObject;

class Register extends DataTransferObject
{
    /**
     * @var string
     */
    public string $name;

    /**
     * @var string
     */
    public string $email;

    /**
     * @var string
     */
    public string $password;

    /**
     * @var string
     */
    public string $c_password;

}