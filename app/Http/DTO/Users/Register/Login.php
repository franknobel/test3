<?php

namespace App\Http\DTO\Users\Register;

use Spatie\DataTransferObject\DataTransferObject;

class Login extends DataTransferObject
{
    /**
     * @var string
     */
    public string $email;

    /**
     * @var string
     */
    public string $password;
    
}