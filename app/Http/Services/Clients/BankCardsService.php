<?php

namespace App\Http\Services\Clients;

use App\Http\Helpers\ResponseHelper;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\ClientFilesImport;
use App\Models\ClientFiles;
use App\Jobs\GetCardInfo;
use App\Jobs\SendClientFile;

class BankCardsService
{
    use ResponseHelper;

    const MAX_TIMEOUT = 10;

    public function checkBankCards()
    {
        $inputs = request()->all();
        $userId = auth()->user()->id;

        $validate = Validator::make($inputs, [
            'back_url' => 'required|url',
            'file' => 'required|file'
        ]);

        if($validate->fails()){
            return $this->sendError('Validation Error.', $validate->errors());       
        }

        if(false === $this->importFile()) {
            return $this->sendError('Import Error'); 
        }

        $lastFile = ClientFiles::whereUserId($userId)->first();

        // Send queue
        dispatch(new GetCardInfo($lastFile->id));

    }

    /**
     * @param mixed $clientFileId
     * @return void
     */
    public function SendClientFile($clientFileId)
    {
        dispatch(new SendClientFile($clientFileId));
    }

    /**
     * @return bool
     */
    private function importFile()
    {
        try 
        {
            Excel::import(new ClientFilesImport, request()->file('file'));
        } 
        catch(\Exception $exception) 
        {
            return false;
        }

        return true;
    }

    /**
     * @param string $card
     * @return array|null
     */
    public function getCardInfo(string $card): array|null
    {
        $response = null;
        $apiKey = $this->getApiKey();
        $timeOut = $this->getTimeout();

        if(null === $apiKey) {
            return null;
        }

        $url = "https://api.bintable.com/v1/{$card}?api_key={$apiKey}";
        
        try 
        {
            $response = Http::timeout($timeOut)->get($url)->json();
        } 
        catch(\Exception $exception) 
        {
            return null;
        }

        return $response;
    }

    private function getApiKey(): ?string
    {
        return getenv('BINTABLE_API');
    }

    private function getTimeout(): ?string
    {
        return getenv('BINTABLE_TIMEOUT') ?? 10;
    }
}