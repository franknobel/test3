<?php

namespace App\Http\Services\Users;

use Illuminate\Http\Request;
use App\Models\User;
use App\Http\Helpers\ResponseHelper;
use Illuminate\Http\JsonResponse;
use App\Http\DTO\Users\Register\Register;
use App\Http\DTO\Users\Register\Login;
use Illuminate\Support\Facades\Validator;

class RegisterService
{
    use ResponseHelper;

    /**
     * @var Register
     */
    private Register $registerRequest;

    /**
     * @var Login
     */
    private Login $loginRequest;

    public function register(): JsonResponse
    {
        $inputs = $this->getRegisterRequest()->all();
        
        $validate = Validator::make($inputs, [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
   
        if($validate->fails()){
            return $this->sendError('Validation Error.', $validate->errors());       
        }

        $inputs['password'] = bcrypt($inputs['password']);

        try 
        {
            $user = User::create($inputs);
        }
        catch(\Exception $exception)
        {
            return $this->sendError('Validation Error.', $exception->getMessage());
        }

        return $this->sendResponse([
            'token' => $user->createToken('MyApp')->plainTextToken,
            'name'  => $user->name
        ], 'User register successfully.');
    }

    public function login()
    {
        $input = $this->getLoginRequest();

        if(auth()
            ->attempt(
                [
                    'email' => $input->email, 
                    'password' => $input->password
                ]
            )
        ) { 
            $user = auth()->user(); 

            return $this->sendResponse([
                'token' => $user->createToken('MyApp')->plainTextToken,
                'name' => $user->name
            ], 'User login successfully.');
        } 
        
        return $this->sendError('Unauthorised.', ['error'=>'Unauthorised']); 
    }

    /**
     * @return Register
     */
    private function getRegisterRequest(): Register
    {
        return new Register(request()->all());
    }

    /**
     * @return Login
     */
    private function getLoginRequest(): Login
    {
        return new Login(request()->all());
    }

}