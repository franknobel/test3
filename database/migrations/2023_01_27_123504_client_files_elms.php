<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_files_elms', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('client_files_id')->unsigned();
            $table->string('client_id')->nullable();
            $table->string('info')->nullable();
            $table->string('card')->nullable();
            $table->string('type')->nullable();
            $table->text('bank')->nullable();
            $table->timestamps();

            $table->foreign('client_files_id')->references('id')->on('client_files');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('client_files_elms');
    }
};
